package boot

import (
	"github.com/gogf/gf/frame/g"
	_ "millet-go/packed"
	"os"
)

func init() {
	println("  _____ _______      ___      ___           ___           _______       __________")
	println(" |\\   _ \\  _   \\    |\\  \\    |\\  \\         |\\  \\         |\\  ___ \\     |\\___   ___\\")
	println(" \\ \\  \\\\__\\ \\   \\   \\ \\  \\   \\ \\  \\        \\ \\  \\        \\ \\   __/|    \\|___ \\  \\_|")
	println("  \\ \\  \\|__| \\   \\   \\ \\  \\   \\ \\  \\        \\ \\  \\        \\ \\  \\_|/__       \\ \\  \\")
	println("   \\ \\  \\    \\ \\  \\   \\ \\  \\   \\ \\  \\____    \\ \\  \\____    \\ \\  \\_|\\ \\       \\ \\  \\")
	println("    \\ \\__\\    \\ \\__\\   \\ \\__\\   \\ \\_______\\   \\ \\_______\\   \\ \\_______\\       \\ \\__\\")
	println("     \\|__|     \\|__|    \\|__|    \\|_______|    \\|_______|    \\|_______|        \\|\\__|")

	SharePath, exists := os.LookupEnv("SHARE_PATH")
	if exists {
		g.Config().Set("SHARE_PATH", SharePath)
	} else {
		g.Config().Set("SHARE_PATH", "/data")
	}
}
