package router

import (
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/gsession"
	"millet-go/app/api/admin"
	"millet-go/app/api/front"
	"millet-go/router/middleware"
)

func init() {
	s := g.Server()
	_ = s.SetConfigWithMap(g.Map{
		"SessionStorage": gsession.NewStorageRedisHashTable(g.Redis()),
	})
	s.Group("/", func(group *ghttp.RouterGroup) {
		group.Middleware(middleware.MiddlewareCORS)
		group.POST("/register", front.User.Register)
		group.POST("/login", front.User.Login)
		group.Middleware(middleware.MiddlewareAuth)
		group.Group("/api", func(group *ghttp.RouterGroup) {
			group.Group("/user", func(group *ghttp.RouterGroup) {
				group.GET("/info", front.User.GetSelfInfo)
			})
			group.Group("/group", func(group *ghttp.RouterGroup) {
				group.GET("/", front.Group.GroupList)
				group.POST("/", front.Group.Create)
				group.GET("/:code", front.Group.GetGroupInfo)
				group.PUT("/:code", front.Group.UpdateGroup)
				group.POST("/join/:code", front.Group.JoinGroup)
			})
			group.Group("/trade", func(group *ghttp.RouterGroup) {
				group.GET("/", front.Trade.TradeList)
				group.POST("/", front.Trade.Create)
				group.GET("/:code", front.Trade.GetTradeInfo)
				group.PUT("/:code", front.Trade.UpdateTrade)
				group.POST("/join/:code", front.Trade.JoinTrade)
				group.POST("/start", front.Trade.TradeStart)
				group.GET("/getStatus", front.Trade.GetStatus)
				group.POST("/uploadStatus", front.Trade.PostTrade)
			})
			group.Group("/millet", func(group *ghttp.RouterGroup) {
				group.GET("/", front.Millet.MilletList)
				group.POST("/", front.Millet.Create)
			})
			group.Group("/request", func(group *ghttp.RouterGroup) {
				group.POST("/solve", front.Request.SolveRequest)
			})
		})
		group.Middleware(middleware.MiddlewareAdmin)
		group.Group("/admin", func(group *ghttp.RouterGroup) {
			group.Group("/user", func(group *ghttp.RouterGroup) {
				group.GET("/", admin.Admin.UserList)
				group.GET("/:phone", admin.Admin.UserInfo)
			})
			group.Group("/group", func(group *ghttp.RouterGroup) {
				group.GET("/", admin.Admin.GroupList)
				group.GET("/:code", admin.Admin.GroupInfo)
			})
			group.Group("/trade", func(group *ghttp.RouterGroup) {
				group.GET("/", admin.Admin.TradeList)
				group.GET("/:code", admin.Admin.TradeInfo)
			})
			group.Group("/cdkey", func(group *ghttp.RouterGroup) {
				group.GET("/", admin.Admin.CDKeyList)
				group.POST("/", admin.Admin.GenertorCDKey)
			})
		})
	})
}
