package middleware

import (
	"github.com/gogf/gf/net/ghttp"
	"net/http"
)

func MiddlewareAdmin(r *ghttp.Request) {
	IsSuperuser := r.Session.GetBool("is_superuser")
	if IsSuperuser {
		r.Middleware.Next()
	} else {
		r.Response.WriteStatus(http.StatusForbidden)
	}
}
