package middleware

import (
	"github.com/gogf/gf/net/ghttp"
	"net/http"
)

func MiddlewareAuth(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	if phone != "" {
		r.Middleware.Next()
	}else{
		r.Response.WriteStatus(http.StatusUnauthorized)
	}
}