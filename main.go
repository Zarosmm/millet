package main

import (
	_ "millet-go/boot"
	_ "millet-go/router"

	"github.com/gogf/gf/frame/g"
)

func main() {
	g.Server().Run()
}
