package packed

import "github.com/gogf/gf/os/gres"

func init() {
	if err := gres.Add("H4sIAAAAAAAC/wrwZmYRYeBg4GAwMPgdxIAERBg4GYrLE9PTU4v0obReVnF+XmgIKwOjzcL/CaueBQUEBARo6fn7+PvrBm0KCjC6ZBxwqbEj7dnSp9qRmg5XGlwnff7sJTTZ26Fpa8LFqBWXmo5M6knqKNBVYGD4/z/Am53DrHPCQgcGBoYQBgYGmFMYGAzRnMKOcArYdruF/xNAupHVBHgzMokwI7yCbDLIKzCwrRFE4vUYwijsToEAAYb/jt0Io5AcxsoGkmdiYGLoZGBgOAlWDQgAAP//ECXdl2gBAAA="); err != nil {
		panic("add binary content to resource manager failed: " + err.Error())
	}
}
