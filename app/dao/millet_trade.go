// =================================================================================
// This is auto-generated by GoFrame CLI tool only once. Fill this file as you wish.
// =================================================================================

package dao

import (
	"millet-go/app/dao/internal"
)

// milletTradeDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type milletTradeDao struct {
	*internal.MilletTradeDao
}

var (
	// MilletTrade is globally public accessible object for table millet_trade operations.
	MilletTrade milletTradeDao
)

func init() {
	MilletTrade = milletTradeDao{
		internal.NewMilletTradeDao(),
	}
}

// Fill with you ideas below.
