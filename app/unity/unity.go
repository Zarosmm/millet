package unity

import (
	"millet-go/app/serializer"
)

func GetOffset(p *serializer.PageReq) (int, int, error) {
	offset := 0
	limit := 10
	if p.Limit > 0 && p.Page >= 1 {
		limit = p.Limit
		offset = p.Limit * (p.Page - 1)
	}
	return offset, limit, nil
}
