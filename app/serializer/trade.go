package serializer

import (
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gmeta"
)

type TradeInfoUpdateReq struct {
	Name        string `orm:"name"              json:"name"`        //
	Description string `orm:"description"       json:"description"` //
	Status      string `orm:"status"            json:"status"`      //
}

type TradeAddReq struct {
	Name        string `json:"name"        p:"name"        v:"required"#请输入交易名`  //
	Description string `json:"description" p:"description" v:"required"#请输入交易描述` //
	Cdkey       string `json:"cdkey"       p:"cdkey"       v:"required"#请输入交易码`  //
}

type NewTrade struct {
	Name            string `orm:"name"              json:"name"`            //
	Description     string `orm:"description"       json:"description"`     //
	MaxPeopleNumber int    `orm:"max_people_number" json:"maxPeopleNumber"` //
	CdkeyId         int    `orm:"cdkey_id"          json:"cdkeyId"`         //
	CreateById      int    `orm:"create_by_id"      json:"createById"`      //
	GroupId         int    `orm:"group_id"          json:"groupId"`         //
	Code            string `orm:"code"              json:"code"`            //
}

type TradeInfo struct {
	Id              int    `orm:"id,primary"        json:"id"`              //
	Name            string `orm:"name"              json:"name"`            //
	Description     string `orm:"description"       json:"description"`     //
	MaxPeopleNumber int    `orm:"max_people_number" json:"maxPeopleNumber"` //
	Status          string `orm:"status"            json:"status"`          //
	Code            string `orm:"code"              json:"code"`
}

type TradeDeepInfo struct {
	gmeta.Meta      `orm:"table:trade"`
	Id              int                `orm:"id,primary"        json:"id"`              //
	Name            string             `orm:"name"              json:"name"`            //
	Description     string             `orm:"description"       json:"description"`     //
	MaxPeopleNumber int                `orm:"max_people_number" json:"maxPeopleNumber"` //
	AveragePrice    float64            `orm:"average_price"     json:"averagePrice"`    //
	Editable        int                `orm:"editable"          json:"editable"`        //
	Status          int                `orm:"status"            json:"status"`          //
	IsDelete        int                `orm:"is_delete"         json:"isDelete"`        //
	CreateTime      *gtime.Time        `orm:"create_time"       json:"createTime"`      //
	UpdateTime      *gtime.Time        `orm:"update_time"       json:"updateTime"`      //
	CdkeyId         int                `orm:"cdkey_id"          json:"cdkeyId"`         //
	CreateById      int                `orm:"create_by_id"      json:"createById"`      //
	GroupId         int                `orm:"group_id"          json:"groupId"`         //
	Code            string             `orm:"code"              json:"code"`            //
	Member          []*UserSafeProfile `json:"members"`
	Request         []*RequestInfo     `json:"requests"`
}
