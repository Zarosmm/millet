package serializer

type NewRequest struct {
	Entity   string `orm:"entity"      json:"entity"`   //
	EntityId int    `orm:"entity_id"   json:"entityId"` //
	Info     string `orm:"info"        json:"info"`     //
	Status   int    `orm:"status"      json:"status"`   //
	UserId   int    `orm:"user_id"     json:"userId"`   //
}

type RequestInfo struct {
	Id     int    `orm:"id"       json:"id"`     //
	Info   string `orm:"info"     json:"info"`   //
	Status int    `orm:"status"   json:"status"` //
	UserId int    `orm:"user_id"  json:"userId"` //
}

type SolveRequest struct {
	Id     int `orm:"id"       json:"id"`     //
	Status int `orm:"status"   json:"status"` //
}
