package serializer

import "github.com/gogf/gf/os/gtime"

type MilletAddReq struct {
	Name    string  `orm:"name"         json:"name"`    //
	Price   float64 `orm:"price"        json:"price"`   //
	Stock   int     `orm:"stock"        json:"stock"`   //
	TradeId int     `orm:"trade_id"     json:"tradeId"` //
}

type NewMillet struct {
	Name       string  `orm:"name"         json:"name"`       //
	Url        string  `orm:"url"          json:"url"`        //
	Price      float64 `orm:"price"        json:"price"`      //
	Stock      int     `orm:"stock"        json:"stock"`      //
	CreateById int     `orm:"create_by_id" json:"createById"` //
	TradeId    int     `orm:"trade_id"     json:"tradeId"`    //
}

type MilletInfo struct {
	Id         int         `orm:"id,primary"   json:"id"`         //
	Name       string      `orm:"name"         json:"name"`       //
	Url        string      `orm:"url"          json:"url"`        //
	Price      float64     `orm:"price"        json:"price"`      //
	Stock      int         `orm:"stock"        json:"stock"`      //
	CreateTime *gtime.Time `orm:"create_time"  json:"createTime"` //
	UpdateTime *gtime.Time `orm:"update_time"  json:"updateTime"` //
}
