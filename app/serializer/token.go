package serializer

import "github.com/gogf/gf/os/gtime"

type NewKey struct {
	Token      string `orm:"token,unique" json:"token"`     //
	Code       string `orm:"code"         json:"code"`      //
	IsActive   int    `orm:"is_active"    json:"is_active"` //
	Level      int    `orm:"level"        json:"level"`     //
	CreateById int    `orm:"create_by_id" json:"create_by"` //
}

type KeyInfo struct {
	Id         int         `orm:"id,primary"   json:"id"`         //
	Token      string      `orm:"token,unique" json:"token"`      //
	Code       string      `orm:"code"         json:"code"`       //
	CreateTime *gtime.Time `orm:"create_time"  json:"createTime"` //
	UpdateTime *gtime.Time `orm:"update_time"  json:"updateTime"` //
	IsActive   int         `orm:"is_active"    json:"isActive"`   //
	Level      int         `orm:"level"        json:"level"`      //
	CreateById int         `orm:"create_by_id" json:"createById"` //
}
