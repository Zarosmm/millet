package serializer

type TradeLogAddReq struct {
	Info       []*TradeStatusMillet `json:"info"`                          //
	CreateById int                  `orm:"create_by_id" json:"createById"` //
	TradeId    int                  `orm:"trade_id"     json:"tradeId"`    //
}

type TradeStatus struct {
	Table interface{} `json:"table"`
	Sum   int         `json:"sum"`
}

type TradeStatusUser struct {
	Cols     []*TradeStatusMillet `json:"cols"`
	Username string               `json:"username"`
	Sum      int                  `json:"sum"`
}

type TradeStatusMillet struct {
	Count int    `json:"count"`
	Name  string `json:"name"`
	Stock int    `json:"stock"`
	Price int    `json:"price"`
	Sum   int    `json:"sum"`
}
