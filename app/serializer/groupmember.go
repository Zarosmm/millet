package serializer

import "github.com/gogf/gf/util/gmeta"

type NewGroupMember struct {
	GroupId int `orm:"group_id"     json:"gid"`
	UserId  int `orm:"user_id"      json:"uid"`
}
type GroupMemberInfo struct {
	gmeta.Meta `orm:"table:user_group"`
	GroupId    int          `orm:"group_id"           json:"gid"`
	MemberInfo *UserProfile `orm:"with:id=user_id"    json:"info"`
}
