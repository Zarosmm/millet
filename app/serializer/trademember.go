package serializer

import "github.com/gogf/gf/util/gmeta"

type NewTradeMember struct {
	TradeId int `orm:"trade_id"         json:"tid"`
	UserId  int `orm:"user_id"          json:"uid"`
}
type TradeMemberInfo struct {
	gmeta.Meta `orm:"table:user_group"`
	TradeId    int          `orm:"trade_id"         json:"tid"`
	MemberInfo *UserProfile `orm:"with:id=user_id"  json:"info"`
}

type NewTradeMillet struct {
	TradeId  int `orm:"trade_id"     json:"tid"`
	MilletId int `orm:"millet_id"      json:"mid"`
}
