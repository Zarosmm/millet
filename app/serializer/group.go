package serializer

import (
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gmeta"
)

type GroupInfoUpdateReq struct {
	Name        string `json:"name"		p:"name"		v:"required"#请输入团名`
	Description string `json:"description"	p:"description"	v:"required"#请输入团描述`
}

type GroupAddReq struct {
	Name        string `json:"name"        p:"name"        v:"required"#请输入团名`
	Description string `json:"description" p:"description" v:"required"#请输入团描述`
}

type NewGroup struct {
	Name        string `orm:"name"         json:"name"`        //
	Description string `orm:"description"  json:"description"` //
	CreateById  int    `orm:"create_by_id" json:"createById"`  //
	LeaderId    int    `orm:"leader_id"    json:"leaderId"`    //
}

type GroupInfo struct {
	gmeta.Meta  `orm:"table:group"`
	Id          int              `json:"id"orm:"id"`
	Name        string           `json:"name"`
	Description string           `json:"description"`
	CreateTime  *gtime.Time      `json:"createTime"`
	UpdateTime  *gtime.Time      `json:"updateTime"`
	Code        string           `json:"code"orm:"code"`
	LeaderId    int              `json:"leader_id" orm:"leader_id"`
	LeaderInfo  *UserSafeProfile `json:"leader"orm:"with:id=leader_id"`
}
type GroupDeepInfo struct {
	gmeta.Meta  `orm:"table:group"`
	Id          int                `json:"id"orm:"id"`
	Name        string             `json:"name"`
	Description string             `json:"description"`
	CreateTime  *gtime.Time        `json:"createTime"`
	UpdateTime  *gtime.Time        `json:"updateTime"`
	LeaderId    int                `json:"leader_id" orm:"leader_id"`
	LeaderInfo  *UserSafeProfile   `json:"leader"orm:"with:id=leader_id"`
	Code        string             `json:"code"`
	Trade       []*TradeInfo       `json:"trades"`
	Member      []*UserSafeProfile `json:"members"`
	Request     []*RequestInfo     `json:"requests"`
}
