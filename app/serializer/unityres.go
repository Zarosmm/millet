package serializer

type Res struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

type PageRes struct {
	Count int         `json:"count"`
	Page  int         `json:"page"`
	Limit int         `json:"limit"`
	Code  int         `json:"code"`
	Msg   string      `json:"msg"`
	Data  interface{} `json:"data"`
}
