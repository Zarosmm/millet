package serializer

import (
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gmeta"
)

type RegisterReq struct {
	Username string `json:"username"      p:"username"      v:"required|length:2,32 #请输入账号|账号长度为:min到:max位"`
	Email    string `json:"email"         p:"email"         v:"required				#请输入邮箱"`
	Phone    string `json:"phone"         p:"phone"         v:"required	            #请输入手机号"`
	Password string `json:"password"      p:"password"      v:"required|length:6,16	#请输入密码|密码长度不够"`
	Qq       string `json:"qq"            p:"qq"`
}

type RegisterData struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Phone    string `json:"phone"`
}
type UpdatePasswordReq struct {
	Password    string `p:"password"`
	Newpassword string `p:"newpassword"`
}

type UpdatePasswordServiceReq struct {
	Password string `p:"password"`
}

type LoginReq struct {
	Phone    string `json:"phone"       p:"phone"        v:"required	            #请输入手机号"`
	Password string `json:"password"    p:"password"     v:"required|length:6,16	#请输入密码|密码长度不够"`
}
type LastLoginReq struct {
	LastLogin *gtime.Time `orm:"last_login"   json:"last_login"` //
}
type UserProfile struct {
	gmeta.Meta  `orm:"table:user"`
	Id          int         `orm:"id,primary"   json:"id"`           //
	Username    string      `orm:"username"     json:"username"`     //
	Email       string      `orm:"email"        json:"email"`        //
	IsSuperuser bool        `orm:"is_superuser" json:"is_superuser"` //
	IsActive    bool        `orm:"is_active"    json:"is_active"`    //
	Phone       string      `orm:"phone"        json:"phone"`        //
	LastLogin   *gtime.Time `orm:"last_login"   json:"last_login"`   //
}

type UserSafeProfile struct {
	gmeta.Meta `orm:"table:user"`
	Id         int         `orm:"id,primary"   json:"id"`         //
	Username   string      `orm:"username"     json:"username"`   //
	Email      string      `orm:"email"        json:"email"`      //
	LastLogin  *gtime.Time `orm:"last_login"   json:"last_login"` //
}
