package serializer

type PageReq struct {
	Page  int `json:"page"    p:"page"`
	Limit int `json:"limit"   p:"limit"`
}
