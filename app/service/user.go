package service

import (
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/gogf/gf/crypto/gaes"
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/os/gtime"
	"github.com/gogf/gf/util/gconv"
	"millet-go/app/dao"
	"millet-go/app/serializer"
	"millet-go/app/unity"
)

var User = userService{}

type userService struct{}

func (s *userService) List(p *serializer.PageReq) ([]*serializer.UserProfile, int, error) {
	offset, limit, err := unity.GetOffset(p)
	if err != nil {
		return nil, 0, err
	}
	var users []*serializer.UserProfile
	if err := dao.User.WithAll().Offset(offset).Limit(limit).Scan(&users); err != nil {
		return nil, 0, err
	}
	count, _ := dao.User.Count()
	return users, count, nil
}

func (s *userService) GetProfile(phone string) (*serializer.UserProfile, error) {
	// 获取用户资料
	user, _ := dao.User.FindOne("phone", phone)
	var profile *serializer.UserProfile
	if err := gconv.Struct(user, &profile); err != nil {
		return nil, err
	}
	return profile, nil
}
func (s *userService) GetSafeProfile(phone string) (*serializer.UserSafeProfile, error) {
	// 获取前端可用用户信息
	user, _ := dao.User.FindOne("phone", phone)
	var profile *serializer.UserSafeProfile
	if err := gconv.Struct(user, &profile); err != nil {
		return nil, err
	}
	return profile, nil
}
func (s *userService) SignUp(r *serializer.RegisterReq) error {
	// 注册用户
	if !s.CheckPhoneUnique(r.Phone) {
		return errors.New(fmt.Sprintf("账号 %s 已经存在", r.Phone))
	}
	r.Password = s.PrePassword(r.Phone, r.Password)
	if _, err := dao.User.Insert(r); err != nil {
		return err
	}
	return nil
}

func (s *userService) SignIn(r *serializer.LoginReq) error {
	// 用户登录
	user, _ := dao.User.FindOne("phone", r.Phone)
	if !s.CheckPassword(user, r.Password) {
		return errors.New(fmt.Sprintf("用户名或密码错误"))
	}
	if !s.CheckActive(user) {
		return errors.New(fmt.Sprintf("用户未激活"))
	}
	return nil
}

func (s *userService) UpdateLastLogin(phone string) error {
	// 更新登录记录
	var lastLogin serializer.LastLoginReq
	lastLogin.LastLogin = gtime.Now()
	if _, err := dao.User.Data(lastLogin).Where("phone", phone).Update(); err != nil {
		println(err.Error())
		return errors.New(fmt.Sprintf("请联系管理员反馈问题"))
	}
	return nil
}
func (s *userService) UpdatePassword(phone string, r *serializer.UpdatePasswordReq) error {
	// 更新密码
	user, _ := dao.User.FindOne("phone", phone)
	if !s.CheckPassword(user, r.Password) {
		return errors.New(fmt.Sprintf("密码验证错误"))
	}
	var serviceReq serializer.UpdatePasswordServiceReq
	serviceReq.Password = s.PrePassword(phone, r.Password)
	if _, err := dao.User.Data(serviceReq).Where("phone", phone).Update(); err != nil {
		println(err.Error())
		return errors.New(fmt.Sprintf("密码更改失败"))
	}
	return nil
}

func (s *userService) CheckPhoneUnique(phone string) bool {
	// 检查手机号是否重复
	if i, err := dao.User.FindCount("phone", phone); err != nil {
		return false
	} else {
		return i == 0
	}
}

func (s *userService) CheckPassword(user gdb.Record, password string) bool {
	// 验证密码
	return s.PrePassword(user["phone"].String(), password) == user["password"].String()
}

func (s *userService) CheckActive(user gdb.Record) bool {
	// 检查用户激活
	return user["is_active"].Bool()
}

func (s *userService) PreSalt(phone string) []byte {
	// 准备密码盐
	key := g.Config().GetBytes("salt")
	salt, _ := gaes.Encrypt([]byte(phone), key)
	return salt
}

func (s *userService) PrePassword(phone string, password string) string {
	// 明文密码加密
	salt := s.PreSalt(phone)
	secret, _ := gaes.Encrypt([]byte(password), salt)
	password = base64.StdEncoding.EncodeToString(secret)
	return password
}
