package service

import (
	"errors"
	"fmt"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
	"millet-go/app/dao"
	"millet-go/app/serializer"
	"millet-go/app/unity"
)

var Group = groupService{}

type groupService struct{}

func (s *groupService) List(p *serializer.PageReq) ([]*serializer.GroupInfo, int, error) {
	offset, limit, err := unity.GetOffset(p)
	if err != nil {
		return nil, 0, err
	}
	var groups []*serializer.GroupInfo
	if err := dao.Group.WithAll().Offset(offset).Limit(limit).Scan(&groups); err != nil {
		return nil, 0, err
	}
	count, _ := dao.Group.Count()
	return groups, count, nil
}
func (s *groupService) Create(g *serializer.GroupAddReq, phone string) (*serializer.GroupInfo, error) {
	user, err := dao.User.FindOne("phone", phone)
	if err != nil {
		return nil, err
	}
	group := new(serializer.NewGroup)
	group.CreateById = user["id"].Int()
	group.LeaderId = user["id"].Int()
	if err := gconv.Struct(g, &group); err != nil {
		return nil, err
	}
	gid, err := dao.Group.InsertAndGetId(group)
	code := unity.InviteCode.Encode(uint64(gid))
	_, _ = dao.Group.Where("id", gid).Data("code", code).Update()
	if err != nil {
		return nil, err
	}
	member := new(serializer.NewGroupMember)
	member.UserId = user["id"].Int()
	member.GroupId = int(gid)
	_, err = dao.UserGroup.Insert(member)
	var groupInfo *serializer.GroupInfo
	if err := dao.Group.WithAll().Where("code", code).Scan(&groupInfo); err != nil {
		return nil, err
	}
	return groupInfo, nil
}

func (s *groupService) Update(g *serializer.GroupInfoUpdateReq, code string) error {
	if _, err := dao.Group.Data(g).Where("code", code).Update(); err != nil {
		println(err.Error())
		return errors.New(fmt.Sprintf("信息修改失败"))
	}
	return nil
}

func (s *groupService) GetInfo(code string) (*serializer.GroupDeepInfo, error) {
	var groups *serializer.GroupDeepInfo
	count, _ := dao.Group.Where("code", code).Count()
	if count == 0 {
		return nil, errors.New(fmt.Sprintf("该code无对应组"))
	}
	if err := dao.Group.WithAll().Where("code", code).Scan(&groups); err != nil {
		return nil, err
	}

	trades, _ := dao.Trade.Where("group_id", groups.Id).All()
	var gtrades []*serializer.TradeInfo
	if err := gconv.Structs(trades, &gtrades); err != nil {
		return nil, err
	}
	groups.Trade = gtrades

	requests, _ := dao.Request.Where("entity", "group").Where("status", 0).Where("entity_id", groups.Id).All()
	var grequests []*serializer.RequestInfo
	if err := gconv.Structs(requests, &grequests); err != nil {
		return nil, err
	}
	groups.Request = grequests

	uids, _ := dao.UserGroup.Fields("user_id").Where("group_id", groups.Id).Array()
	users, _ := dao.User.WhereIn("id", uids).All()
	var members []*serializer.UserSafeProfile
	if err := gconv.Structs(users, &members); err != nil {
		return nil, err
	}
	groups.Member = members
	return groups, nil
}

func (s *groupService) Join(code string, phone string) error {
	group, _ := dao.Group.FindOne("code", code)
	user, _ := dao.User.FindOne("phone", phone)
	if group["leader_id"].Int() == user["id"].Int() {
		return errors.New(fmt.Sprintf("你搞咩啊"))
	}
	req, _ := dao.Request.Where(g.Map{
		"entity":    "group",
		"entity_id": group["id"].Int(),
		"user_id":   user["id"].Int(),
	}).WhereIn("status", g.Array{0, 1}).All()
	if !req.IsEmpty() {
		return errors.New(fmt.Sprintf("您已加入或已发送请求"))
	}
	requestS := new(requestService)
	_ = requestS.SendRequest(user["id"].Int(), "group", group["id"].Int(), user["username"].String(), group["name"].String())
	return nil
}
