package service

import (
	"github.com/gogf/gf/database/gdb"
	"github.com/gogf/guuid"
	"hash/crc32"
	"millet-go/app/dao"
	"millet-go/app/serializer"
	"millet-go/app/unity"
	"strconv"
	"strings"
)

var CDKey = cdKeyService{}

type cdKeyService struct{}

func (s *cdKeyService) List(p *serializer.PageReq) ([]*serializer.KeyInfo, int, error) {
	offset, limit, err := unity.GetOffset(p)
	if err != nil {
		return nil, 0, err
	}
	var cdKeyLists []*serializer.KeyInfo
	if err := dao.Cdkey.WithAll().Offset(offset).Limit(limit).Scan(&cdKeyLists); err != nil {
		return nil, 0, err
	}
	count, _ := dao.Cdkey.Count()
	return cdKeyLists, count, nil
}

func (s *cdKeyService) GenertorCDKey(phone string, level int) (*serializer.NewKey, error) {
	token := guuid.New().String()
	user, _ := dao.User.FindOne("phone", phone)
	key := new(serializer.NewKey)
	key.Token = token
	key.Level = level
	key.IsActive = 1
	key.CreateById = user["id"].Int()
	key.Code = s.GenertorCode(strings.Replace(token, "-", "", -1))
	_, err := dao.Cdkey.Insert(key)
	if err != nil {
		return nil, err
	}
	return key, nil
}

func (s *cdKeyService) GenertorCode(token string) string {
	u32code := crc32.ChecksumIEEE([]byte(token))
	code := strconv.FormatUint(uint64(u32code), 10)
	return code
}

func (s *cdKeyService) GetEntityByCode(code string, typ string) gdb.Record {
	var entity gdb.Record
	switch typ {
	case "group":
		entity, _ = dao.Group.FindOne("code", code)
	case "trade":
		entity, _ = dao.Trade.FindOne("code", code)
	default:
		return nil
	}
	return entity
}
