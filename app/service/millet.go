package service

import (
	"errors"
	"fmt"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
	"millet-go/app/dao"
	"millet-go/app/serializer"
	"millet-go/app/unity"
)

var Millet = milletService{}

type milletService struct{}

func (s *milletService) List(p *serializer.PageReq, tcode string) ([]*serializer.MilletInfo, int, error) {
	offset, limit, err := unity.GetOffset(p)
	if err != nil {
		return nil, 0, err
	}
	var milletLists []*serializer.MilletInfo
	var trades *serializer.TradeDeepInfo
	count, _ := dao.Trade.Where("code", tcode).Count()
	if count == 0 {
		return nil, 0, errors.New(fmt.Sprintf("该code无对应交易"))
	}
	if err := dao.Trade.WithAll().Where("code", tcode).Scan(&trades); err != nil {
		return nil, 0, err
	}
	if err := dao.Millet.Where("trade_id", trades.Id).Offset(offset).Limit(limit).Scan(&milletLists); err != nil {
		return nil, 0, err
	}
	count, _ = dao.Millet.Count()
	return milletLists, count, nil
}

func (s *milletService) Create(m *serializer.MilletAddReq, phone string, file *ghttp.UploadFile) (*serializer.NewMillet, error) {
	user, err := dao.User.FindOne("phone", phone)
	if err != nil {
		return nil, err
	}
	trade, err := dao.Trade.FindOne("id", m.TradeId)
	if err != nil {
		return nil, err
	}
	if trade["status"].Int() != 0 {
		return nil, errors.New(fmt.Sprintf("交易未在准备阶段,无法添加"))
	}
	path := g.Config().GetString("SHARE_DATA") + "/" +
		trade["group_id"].String() + "/" +
		trade["id"].String() + "/"
	filename, err := file.Save(path, true)
	if err != nil {
		return nil, err
	}
	millet := new(serializer.NewMillet)
	millet.Url = filename
	millet.CreateById = user["id"].Int()
	if err := gconv.Struct(m, &millet); err != nil {
		return nil, err
	}
	mid, err := dao.Millet.InsertAndGetId(millet)
	if err != nil {
		return nil, err
	}
	trademillet := new(serializer.NewTradeMillet)
	trademillet.TradeId = trade["id"].Int()
	trademillet.MilletId = int(mid)
	_, err = dao.MilletTrade.Insert(trademillet)
	averageprice, _ := dao.Millet.Where("trade_id", trade["id"].Int()).Avg("price")
	_, _ = dao.Trade.Where("id", trade["id"].Int()).Data("average_price", averageprice).Update()
	if err != nil {
		return nil, err
	}
	return millet, nil
}
