package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/util/gconv"
	"millet-go/app/dao"
	"millet-go/app/serializer"
	"millet-go/app/unity"
)

var Trade = tradeService{}

type tradeService struct{}

func (s *tradeService) List(p *serializer.PageReq, gcode string) ([]*serializer.TradeInfo, int, error) {
	offset, limit, err := unity.GetOffset(p)
	if err != nil {
		return nil, 0, err
	}
	var trades []*serializer.TradeInfo
	count, _ := dao.Group.Where("code", gcode).Count()
	if count == 0 {
		return nil, 0, errors.New(fmt.Sprintf("该code无对应团"))
	}
	group, _ := dao.Group.Where("code", gcode).FindOne()
	if err := dao.Trade.Where("group_id", group["id"].Int()).Offset(offset).Limit(limit).Scan(&trades); err != nil {
		return nil, 0, err
	}
	count, _ = dao.Trade.Where("group_id", group["id"].Int()).Count()
	return trades, count, nil
}
func (s *tradeService) Create(g *serializer.TradeAddReq, phone string) (*serializer.NewTrade, error) {
	user, err := dao.User.FindOne("phone", phone)
	group, err := dao.Group.FindOne("leader_id", user["id"].Int())
	if err != nil {
		return nil, err
	}
	cdkey, err := dao.Cdkey.One("token", g.Cdkey)
	if err != nil {
		return nil, err
	}
	if cdkey["is_active"].Int() == 0 {
		return nil, errors.New("invalid cdkey")
	}
	trade := new(serializer.NewTrade)
	trade.Code = unity.InviteCode.Encode(cdkey["code"].Uint64())
	trade.CdkeyId = cdkey["id"].Int()
	trade.CreateById = user["id"].Int()
	trade.GroupId = group["id"].Int()
	trade.MaxPeopleNumber = cdkey["level"].Int() * 20
	if err := gconv.Struct(g, &trade); err != nil {
		return nil, err
	}
	tid, err := dao.Trade.InsertAndGetId(trade)
	if err != nil {
		return nil, err
	}
	_, _ = dao.Cdkey.Data("is_active", 0).Where("id", cdkey["id"].Int()).Update()
	member := new(serializer.NewTradeMember)
	member.UserId = user["id"].Int()
	member.TradeId = int(tid)
	_, err = dao.UserTrade.Insert(member)
	return trade, nil
}

func (s *tradeService) Update(t *serializer.TradeInfoUpdateReq, code string) error {
	if _, err := dao.Trade.Data(t).Where("code", code).Update(); err != nil {
		println(err.Error())
		return errors.New(fmt.Sprintf("信息修改失败"))
	}
	return nil
}

func (s *tradeService) GetInfo(code string) (*serializer.TradeDeepInfo, error) {
	var trades *serializer.TradeDeepInfo
	count, _ := dao.Trade.Where("code", code).Count()
	if count == 0 {
		return nil, errors.New(fmt.Sprintf("该code无对应交易"))
	}
	if err := dao.Trade.WithAll().Where("code", code).Scan(&trades); err != nil {
		return nil, err
	}

	requests, _ := dao.Request.Where("entity", "trade").Where("status", 0).Where("entity_id", trades.Id).All()
	var trequests []*serializer.RequestInfo
	if err := gconv.Structs(requests, &trequests); err != nil {
		return nil, err
	}
	trades.Request = trequests

	uids, _ := dao.UserTrade.Fields("user_id").Where("trade_id", trades.Id).Array()
	users, _ := dao.User.WhereIn("id", uids).All()
	var members []*serializer.UserSafeProfile
	if err := gconv.Structs(users, &members); err != nil {
		return nil, err
	}
	trades.Member = members
	return trades, nil
}

func (s *tradeService) Join(code string, phone string) error {
	trade, _ := dao.Trade.FindOne("code", code)
	if trade["status"].Int() != 0 {
		return errors.New(fmt.Sprintf("该交易已经开始，无法中途加入"))
	}
	user, _ := dao.User.FindOne("phone", phone)
	group, _ := dao.Group.FindOne("id", trade["group_id"].Int())
	if group["leader_id"].Int() == user["id"].Int() {
		return errors.New(fmt.Sprintf("你搞咩啊"))
	}
	req, _ := dao.Request.Where(g.Map{
		"entity":    "trade",
		"entity_id": trade["id"].Int(),
		"user_id":   user["id"].Int(),
	}).WhereIn("status", g.Array{0, 1}).All()
	if !req.IsEmpty() {
		return errors.New(fmt.Sprintf("您已加入或已发送请求"))
	}
	requestS := new(requestService)
	_ = requestS.SendRequest(user["id"].Int(), "trade", trade["id"].Int(), user["username"].String(), trade["name"].String())
	return nil
}

func (s *tradeService) Init(code string) error {
	trade, _ := dao.Trade.Where("code", code).FindOne()
	// Todo
	//  审核
	if _, err := dao.Trade.Data("status", 1).Where("id", trade["id"].Int()).Update(); err != nil {
		return err
	}
	initData := g.Array{}
	members, _ := dao.User.As("u").LeftJoin("user_trade ut", "u.id=ut.user_id").Fields("u.id,username").Where("trade_id", trade["id"].Int()).All()
	millets, _ := dao.Millet.Where(g.Map{"trade_id": trade["id"].Int()}).All()
	column := g.Array{}
	for _, millet := range millets {
		column = append(column, g.Map{
			"name":  millet["name"].String(),
			"count": 0,
			"stock": millet["stock"].Int(),
			"price": millet["price"].Int(),
			"sum":   0,
		})
	}
	for _, member := range members {
		initData = append(initData, g.Map{
			"username": member["username"].String(),
			"cols":     column,
			"sum":      0,
		})
	}
	data := g.Map{"data": initData, "trade_id": trade["id"].Int(), "is_editable": 1}
	if result, err := dao.TradeStatus.Where("trade_id", trade["id"].Int()).One(); result != nil && err == nil {
		data["id"] = result["id"].Int()
	}
	if _, err := dao.TradeStatus.Data(data).Save(); err != nil {
		return err
	}
	return nil
}

func (s *tradeService) GetStatus(code string) (*serializer.TradeStatus, error) {
	trade, _ := dao.Trade.Where("code", code).FindOne()
	tradeinfo, _ := dao.TradeStatus.Where("trade_id", trade["id"].Int()).FindOne()
	sheet := tradeinfo["data"].String()
	table := new(serializer.TradeStatus)
	var rows []*serializer.TradeStatusUser
	if err := json.Unmarshal([]byte(sheet), &rows); err != nil {
		fmt.Println(err.Error())
		return nil, errors.New(fmt.Sprintf("数据格式出错,请联系管理员"))
	}
	for _, row := range rows {
		table.Sum += row.Sum
	}
	table.Table = rows
	return table, nil
}
