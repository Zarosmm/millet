package service

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/gogf/gf/frame/g"
	"millet-go/app/dao"
	"millet-go/app/serializer"
)

var TradeStatus = tradeStatusService{}

type tradeStatusService struct{}

func (s *tradeStatusService) UploadTradeStatus(username string, uid int, log *serializer.TradeLogAddReq) error {
	if uid != log.CreateById {
		return errors.New(fmt.Sprintf("这个情况不可能出现啊,你咋发的数据啊"))
	}

	if tx, err := g.DB("millet").Begin(); err == nil {
		tradeInfo, _ := dao.TradeStatus.TX(tx).Where("trade_id", log.TradeId).LockUpdate().FindOne()
		var userMillets []*serializer.TradeStatusUser
		sheet := tradeInfo["data"].String()
		if err := json.Unmarshal([]byte(sheet), &userMillets); err != nil {
			_ = tx.Rollback()
			return errors.New(fmt.Sprintf("数据格式出错,请联系管理员"))
		}
		for _, userMillet := range userMillets {
			if userMillet.Username == username {
				userMillet.Cols = log.Info
				for _, cell := range userMillet.Cols {
					milletInfo, _ := dao.Millet.Where(g.Map{"trade_id": log.TradeId, "name": cell.Name}).FindOne()
					cell.Price = milletInfo["price"].Int()
					cell.Sum = milletInfo["price"].Int() * cell.Count
					userMillet.Sum += cell.Sum
				}
			}
		}
		_, err = dao.TradeStatus.TX(tx).Where("trade_id", log.TradeId).Data("data", userMillets).Update()
		if err != nil {
			_ = tx.Rollback()
			return errors.New(fmt.Sprintf("数据更新错误,请联系管理员"))
		}
		_ = tx.Commit()
		return nil
	}
	return errors.New(fmt.Sprintf("数据更新错误,请联系管理员"))
}
