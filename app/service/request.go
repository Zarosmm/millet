package service

import (
	"errors"
	"fmt"
	"millet-go/app/dao"
	"millet-go/app/serializer"
)

var Request = requestService{}

type requestService struct{}

func (*requestService) SendRequest(uid int, entity string, gid int, username string, entityname string) error {
	request := new(serializer.NewRequest)
	request.UserId = uid
	request.Entity = entity
	request.EntityId = gid
	request.Info = "<span>" + username + "</span>" + "想要加入" + "<span>" + entityname + "</span>"
	request.Status = 0
	if _, err := dao.Request.Insert(request); err != nil {
		return err
	}
	return nil
}

func (s *requestService) Solve(r *serializer.SolveRequest) error {
	request, _ := dao.Request.FindOne("id", r.Id)
	if r.Status == 1 {
		switch request["entity"].String() {
		case "group":
			groupmember := new(serializer.NewGroupMember)
			groupmember.GroupId = request["entity_id"].Int()
			groupmember.UserId = request["user_id"].Int()
			if _, err := dao.UserGroup.Insert(groupmember); err != nil {
				return err
			}
		case "trade":
			trademember := new(serializer.NewTradeMember)
			trademember.TradeId = request["entity_id"].Int()
			trademember.UserId = request["user_id"].Int()
			if _, err := dao.UserTrade.Insert(trademember); err != nil {
				return err
			}
		default:
			return errors.New(fmt.Sprintf("请联系管理员反馈问题"))
		}
		if _, err := dao.Request.Data(r).Where("id", r.Id).Update(); err != nil {
			return err
		}
	} else if r.Status == 2 {
		if _, err := dao.Request.Data(r).Where("id", r.Id).Update(); err != nil {
			return err
		}
	} else {
		return errors.New(fmt.Sprintf("请联系管理员反馈问题"))
	}
	return nil
}
