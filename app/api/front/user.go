package front

import (
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

var User = userApi{}

type userApi struct{}

func (*userApi) Register(r *ghttp.Request) {
	var req *serializer.RegisterReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := service.User.SignUp(req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	var regData *serializer.RegisterData
	if err := gconv.Struct(req, &regData); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 201,
		Data: regData,
	})
}

func (*userApi) Login(r *ghttp.Request) {
	var req *serializer.LoginReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := service.User.SignIn(req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	profile, _ := service.User.GetProfile(req.Phone)
	if err := service.User.UpdateLastLogin(req.Phone); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := r.Session.SetMap(gconv.Map(profile)); err != nil {
		println(err.Error())
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: profile,
	})
}

func (*userApi) GetSelfInfo(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	profile, err := service.User.GetSafeProfile(phone)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: profile,
	})
}

func (*userApi) UpdatePassword(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	var req *serializer.UpdatePasswordReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := service.User.UpdatePassword(phone, req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})
}
