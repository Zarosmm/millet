package front

import (
	"github.com/gogf/gf/net/ghttp"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

var Millet = milletApi{}

type milletApi struct{}

func (*milletApi) MilletList(r *ghttp.Request) {
	var page *serializer.PageReq
	tcode := r.GetString("tcode")
	if err := r.Parse(&page); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	result, count, err := service.Millet.List(page, tcode)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}

	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code:  200,
		Count: count,
		Data:  result,
	})
}

func (*milletApi) Create(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	var req *serializer.MilletAddReq
	file := r.GetUploadFile("upload-file")
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	millet, err := service.Millet.Create(req, phone, file)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: millet,
	})
}
