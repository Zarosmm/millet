package front

import (
	"github.com/gogf/gf/net/ghttp"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

var Request = requestApi{}

type requestApi struct{}

func (*requestApi) SolveRequest(r *ghttp.Request) {
	solvereq := new(serializer.SolveRequest)
	if err := r.Parse(&solvereq); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := service.Request.Solve(solvereq); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})
}
