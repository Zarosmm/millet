package front

import (
	"github.com/gogf/gf/net/ghttp"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

var Group = groupApi{}

type groupApi struct{}

func (*groupApi) Create(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	var req *serializer.GroupAddReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	group, err := service.Group.Create(req, phone)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: group,
	})
}
func (*groupApi) GroupList(r *ghttp.Request) {
	var page *serializer.PageReq
	if err := r.Parse(&page); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	result, count, err := service.Group.List(page)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}

	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code:  200,
		Count: count,
		Data:  result,
	})
}
func (*groupApi) GetGroupInfo(r *ghttp.Request) {
	code := r.GetString("code")
	profile, err := service.Group.GetInfo(code)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: profile,
	})
}
func (*groupApi) UpdateGroup(r *ghttp.Request) {
	code := r.GetString("code")
	var req *serializer.GroupInfoUpdateReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := service.Group.Update(req, code); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
	})

}
func (*groupApi) JoinGroup(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	code := r.GetString("code")
	err := service.Group.Join(code, phone)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})
}
