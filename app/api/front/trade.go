package front

import (
	"github.com/gogf/gf/net/ghttp"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

var Trade = tradeApi{}

type tradeApi struct{}

func (*tradeApi) Create(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	var req *serializer.TradeAddReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	group, err := service.Trade.Create(req, phone)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: group,
	})
}

func (*tradeApi) TradeList(r *ghttp.Request) {
	var page *serializer.PageReq
	gcode := r.GetString("gcode")
	if err := r.Parse(&page); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	result, count, err := service.Trade.List(page, gcode)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}

	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code:  200,
		Count: count,
		Data:  result,
	})
}
func (*tradeApi) GetTradeInfo(r *ghttp.Request) {
	code := r.GetString("code")
	profile, err := service.Trade.GetInfo(code)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: profile,
	})
}

func (*tradeApi) UpdateTrade(r *ghttp.Request) {
	code := r.GetString("code")
	var req *serializer.TradeInfoUpdateReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	if err := service.Trade.Update(req, code); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})

}
func (*tradeApi) JoinTrade(r *ghttp.Request) {
	phone := r.Session.GetString("phone")
	code := r.GetString("code")
	err := service.Trade.Join(code, phone)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})
}

func (*tradeApi) TradeStart(r *ghttp.Request) {
	code := r.GetString("code")
	err := service.Trade.Init(code)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})
}

func (*tradeApi) GetStatus(r *ghttp.Request) {
	code := r.GetString("code")
	data, err := service.Trade.GetStatus(code)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
		Data: data,
	})
}

func (*tradeApi) PostTrade(r *ghttp.Request) {
	username := r.Session.GetString("username")
	uid := r.Session.GetInt("id")
	var req *serializer.TradeLogAddReq
	if err := r.Parse(&req); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	err := service.TradeStatus.UploadTradeStatus(username, uid, req)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Msg:  "success",
	})
}
