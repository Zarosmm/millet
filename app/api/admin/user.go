package admin

import (
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/util/gconv"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

func (*adminApi) UserList(r *ghttp.Request) {
	var page *serializer.PageReq
	if err := r.Parse(&page); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	result, count, err := service.User.List(page)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	var userLists []*serializer.UserProfile
	if err := gconv.Structs(result, &userLists); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code:  200,
		Count: count,
		Data:  userLists,
	})
}
func (*adminApi) UserInfo(r *ghttp.Request) {
	profile, err := service.User.GetProfile(r.GetString("phone"))
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: profile,
	})
}
