package admin

import (
	"github.com/gogf/gf/net/ghttp"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

func (*adminApi) GenertorCDKey(r *ghttp.Request) {
	phone := r.GetString("phone")
	level := r.GetInt("level")
	cdkey, err := service.CDKey.GenertorCDKey(phone, level)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.PageRes{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code: 200,
		Data: cdkey,
	})
}

func (*adminApi) CDKeyList(r *ghttp.Request) {
	var page *serializer.PageReq
	if err := r.Parse(&page); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	result, count, err := service.CDKey.List(page)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code:  200,
		Count: count,
		Data:  result,
	})
}
