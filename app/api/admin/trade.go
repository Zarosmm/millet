package admin

import (
	"github.com/gogf/gf/net/ghttp"
	"millet-go/app/serializer"
	"millet-go/app/service"
)

func (*adminApi) TradeList(r *ghttp.Request) {
	var page *serializer.PageReq
	gcode := r.GetString("gcode")
	if err := r.Parse(&page); err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	result, count, err := service.Trade.List(page, gcode)
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.PageRes{
		Code:  200,
		Count: count,
		Data:  result,
	})
}

func (*adminApi) TradeInfo(r *ghttp.Request) {
	profile, err := service.Trade.GetInfo(r.GetString("code"))
	if err != nil {
		_ = r.Response.WriteJsonExit(serializer.Res{
			Code: 200,
			Msg:  err.Error(),
		})
	}
	_ = r.Response.WriteJsonExit(serializer.Res{
		Code: 200,
		Data: profile,
	})
}
